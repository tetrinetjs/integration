# Equipe de Integração

## Objetivos:
* Manter dashboard de histórias de usuários c/ respectivos status de issues (board)
* Manter a integração contínua e comunicação entre desenvolvedores

## Grupo 6 - membros
* JOSÉ UMBERTO MOREIRA JÚNIOR - @JoseUmberto (Líder)
* ISRAEL
* JECSON
* PEDRO

## Tarefas da sprint 1 (25/03 a 31/03): 
* Iniciar o dashboard:
    * https://gitlab.com/groups/tetrinetjs/-/boards
* Iniciar o CI/CD:
    * https://gitlab.com/groups/tetrinetjs/-/settings/ci_cd